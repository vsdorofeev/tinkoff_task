package com.dvs.testtask.topuppoints.screens.points.map;

import com.dvs.testtask.topuppoints.screens.BasePresenter;
import com.dvs.testtask.topuppoints.screens.BaseView;

public interface MapContract {

    interface View extends BaseView<Presenter> {




    }

    interface Presenter extends BasePresenter {

    }


}
