package com.dvs.testtask.topuppoints.data.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
//@DatabaseTable(tableName = "partners")
public class PartnerInfo {

//    @DatabaseField(id = true)
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("picture")
    private String picture;

    @SerializedName("url")
    private String url;

    @SerializedName("hasLocations")
    private Boolean hasLocations;

    @SerializedName("isMomentary")
    private Boolean isMomentary;

    @SerializedName("depositionDuration")
    private String depositionDuration;

    @SerializedName("limitations")
    private String limitations;

    @SerializedName("pointType")
    private String pointType;

    @SerializedName("externalPartnerId")
    private String externalPartnerId;

    @SerializedName("description")
    private String description;

    @SerializedName("moneyMin")
    private Long moneyMin;

    @SerializedName("moneyMax")
    private Long moneyMax;

    @SerializedName("hasPreferentialDeposition")
    private Boolean hasPreferentialDeposition;

    @SerializedName("limits")
    private List<Limit> limits;

    @SerializedName("dailyLimits")
    private List<DailyLimit> dailyLimits;




}
