package com.dvs.testtask.topuppoints.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PartnerPoint {

    @SerializedName("externalId")
    private String externalId;

    @SerializedName("partnerName")
    private String partnerName;

    @SerializedName("location")
    private Location location;

    @SerializedName("workHours")
    private String workHours;

    @SerializedName("fullAddress")
    private String fullAddress;


}
