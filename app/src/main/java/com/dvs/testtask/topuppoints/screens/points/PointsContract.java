package com.dvs.testtask.topuppoints.screens.points;

import com.dvs.testtask.topuppoints.screens.BasePresenter;
import com.dvs.testtask.topuppoints.screens.BaseView;

public interface PointsContract {

    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {

    }

}
