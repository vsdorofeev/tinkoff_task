package com.dvs.testtask.topuppoints.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DailyLimit {

    @SerializedName("currency")
    private Currency currency;

    @SerializedName("amount")
    private Long amount;

}
