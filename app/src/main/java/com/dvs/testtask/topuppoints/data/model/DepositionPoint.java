package com.dvs.testtask.topuppoints.data.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
//@DatabaseTable(tableName = "deposition_points")
public class DepositionPoint {

    @SerializedName("externalId")
    private String externalId;

    @SerializedName("partnerName")
    private String partnerName;

    @SerializedName("location")
    private Location location;

    @SerializedName("workHours")
    private String workHours;

    @SerializedName("fullAddress")
    private String fullAddress;

    @SerializedName("phones")
    private String phones;

    @SerializedName("addressInfo")
    private String addressInfo;


}
