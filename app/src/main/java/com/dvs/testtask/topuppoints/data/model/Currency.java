package com.dvs.testtask.topuppoints.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Currency {

    @SerializedName("code")
    private Long code;

    @SerializedName("name")
    private String name;

    @SerializedName("strCode")
    private String strCode;

}
