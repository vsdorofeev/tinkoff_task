package com.dvs.testtask.topuppoints.utils;

public class MapUtils {

    // taken from https://developers.google.com/maps/documentation/android-sdk/views
    public static final int ZOOM_LEVEL_WORLD = 1;
    public static final int ZOOM_LEVEL_LANDMASS = 5;
    public static final int ZOOM_LEVEL_CITY = 10;
    public static final int ZOOM_LEVEL_STREET = 15;
    public static final int ZOOM_LEVEL_BUILDING = 20;


}
