package com.dvs.testtask.topuppoints.screens;

public interface BasePresenter {

    void subscribe();

    void unsubscribe();

}
