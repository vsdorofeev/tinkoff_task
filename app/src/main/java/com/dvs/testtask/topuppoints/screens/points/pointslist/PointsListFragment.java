package com.dvs.testtask.topuppoints.screens.points.pointslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dvs.testtask.topuppoints.R;
import com.dvs.testtask.topuppoints.screens.points.BaseFragment;

public class PointsListFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_point_list, container, false);
        return view;
    }

}
