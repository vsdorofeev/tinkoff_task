package com.dvs.testtask.topuppoints.screens.points.map;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dvs.testtask.topuppoints.R;
import com.dvs.testtask.topuppoints.screens.points.BaseFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;

import static com.dvs.testtask.topuppoints.utils.MapUtils.ZOOM_LEVEL_BUILDING;

public class MapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private static final int MY_LOCATION_REQUEST_CODE = 1;

    private MapView mapView;
    private GoogleMap googleMap;

    private UiSettings uiSettings;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_container));

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;

        googleMap.setMyLocationEnabled(true);

        googleMap.setOnCameraIdleListener(this);

        googleMap.setMaxZoomPreference(ZOOM_LEVEL_BUILDING);

        uiSettings = googleMap.getUiSettings();

        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setZoomControlsEnabled(true);

    }

    @Override
    public void onCameraIdle() {
        showPaymentPoints();
    }

    private void showPaymentPoints() {
        Toast.makeText(this.getActivity(), "OnCameraIdle", Toast.LENGTH_SHORT).show();
    }


}
