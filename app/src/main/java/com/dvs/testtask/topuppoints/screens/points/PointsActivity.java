package com.dvs.testtask.topuppoints.screens.points;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dvs.testtask.topuppoints.R;
import com.dvs.testtask.topuppoints.screens.points.map.MapFragment;
import com.dvs.testtask.topuppoints.screens.points.pointslist.PointsListFragment;

import java.util.Arrays;
import java.util.List;

public class PointsActivity extends AppCompatActivity {

    private PointsMapPageAdapter mPointsMapPageAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        List<BaseFragment> pages = Arrays.asList(new MapFragment(), new PointsListFragment());

        mPointsMapPageAdapter = new PointsMapPageAdapter(getSupportFragmentManager(), pages);

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mPointsMapPageAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_points, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class PointsMapPageAdapter extends FragmentPagerAdapter {

        List<BaseFragment> pages;

        public PointsMapPageAdapter(FragmentManager fm, List<BaseFragment> pages) {
            super(fm);
            this.pages = pages;
        }

        @Override
        public BaseFragment getItem(int position) {
            return pages.get(position);
        }

        @Override
        public int getCount() {
            return pages.size();
        }
    }
}
