package com.dvs.testtask.topuppoints.screens.points.pointslist;


import com.dvs.testtask.topuppoints.screens.BasePresenter;
import com.dvs.testtask.topuppoints.screens.BaseView;

public interface PointListContract {

    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {

    }



}
