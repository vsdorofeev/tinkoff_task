package com.dvs.testtask.topuppoints.screens.points;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {

    private static final String ARG_PAGE = "arg_page";

    private int mPage;

    public BaseFragment() {
    }

    public static BaseFragment newInstance(int page) {
         BaseFragment fragment = new BaseFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPage = getArguments().getInt(ARG_PAGE);
        }
    }

}
