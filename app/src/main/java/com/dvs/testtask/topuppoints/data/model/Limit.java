package com.dvs.testtask.topuppoints.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Limit {

    @SerializedName("currency")
    private Currency currency;

    @SerializedName("min")
    private Long min;

    @SerializedName("max")
    private Long max;

}
